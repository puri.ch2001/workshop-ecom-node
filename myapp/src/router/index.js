import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
// import { component } from "vue/types/umd";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },

  {
    path: "",
    name: "toolbar",

    component: () => import("../views/toolbar.vue"),
    children: [
      {
        path: "/me",
        name: "me",
        component: () => import("../views/Me.vue"),
      },
      {
        path: "/simple",
        name: "simple",
        component: () => import("../views/simple.vue"),
      },
      {
        path: "/grade",
        name: "grade",
        component: () => import("../views/grade.vue"),
      },
      {
        path: "/getapi",
        name: "getapi",
        component: () => import("../views/ApiCon.vue"),
      },
      {
        path: "/home",
        name: "home",
        component: () => import("../views/Home.vue"),
      },
      {
        path: "/apple",
        name: "apple",
        component: () => import("../views/Apple.vue"),
      },
      {
        path: "/order",
        name: "order",
        component: () => import("../views/Order.vue"),
      },
    ],
  },
  {
    path: "/signup",
    name: "signup",
    component: () => import("../views/singUp.vue"),
  },
  // {
  //   path: "/me",
  //   name: "me",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ "../views/Me.vue"),
  // },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
